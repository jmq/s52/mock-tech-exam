function countLetter(letter, sentence) {    

    // Check first whether the letter is a single character.
    const result = letter.length
    
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.    
    return result === 1 ? (sentence.split(letter).length-1) : undefined
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    return !text.match(/([a-z]).*\1/i);    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.

    switch (true) {
        case age < 13: return undefined; break;
        case age >= 13 && age <= 21: return parseFloat(price * .8).toFixed(2); break;
        case age >= 60: return parseFloat(price * .8).toFixed(2); break;
        case age > 22 && age <= 64: return parseFloat(price).toFixed(2); break;
        default: return `age: ${age} is out of bound.`
    }    
}

function findHotCategories(items) {    
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    const result = []
    for(i = 0; i < items.length; i++) {
        if(items[i].stocks === 0 && !result.inlcludes(items[i].category)) {
            result.push(items[i].category)
        }
    }
    console.log(result)
    return result

}
console.log(findHotCategories({ id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }))

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

    let voters = candidateA.concat(candidateB)
    // console.log(voters)

    const count = voters => voters.reduce((a, b) => ({ ...a,
    [b]: (a[b] || 0) + 1
    }), {}) // don't forget to initialize the accumulator

    const duplicates = dict =>
    Object.keys(dict).filter((a) => dict[a] > 1)

    console.log(count(voters)) // { Mike: 1, Matt: 1, Nancy: 2, Adam: 1, Jenny: 1, Carl: 1 }
    return duplicates(count(voters)) // [ 'Nancy' ]    
}
    // let a = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // let b = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
    // console.log(findFlyingVoters(a, b))

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};